###############################################################################
#              MAKEFILE FOR EASILY BUILDING A REPORT WITH PANDOC              #
###############################################################################

#############
# VARIABLES #
#############

# The name of the file containing the metadata of the report.
# It may be left blank if these data are put in one of the Markdown files.
METADATA =

# The names of the Markdown source files to build the report from.
FILES =

# The options to pass to Pandoc when converting from Markdown to LaTeX.
# See Pandoc's documentation for more details.
PANDOC_OPTS =

# The name of the output file (without its extension).
OUTPUT =

# The engine used to produce the PDF from the LaTeX source.
PDF_ENGINE = xelatex

###########
# TARGETS #
###########

# Declares non-file targets.
.PHONY: clean mrproper help

# Builds the report from the generated LaTeX source file.
$(OUTPUT).pdf: $(OUTPUT).tex
	$(PDF_ENGINE) -interaction=nonstopmode $(OUTPUT).tex
	grep citation $(OUTPUT).aux && bibtex $(OUTPUT)
	$(PDF_ENGINE) -interaction=nonstopmode $(OUTPUT).tex
	$(PDF_ENGINE) -interaction=nonstopmode $(OUTPUT).tex

# Converts the Markdown source files into LaTeX.
$(OUTPUT).tex: horizontal-lines.pandoc $(METADATA) $(FILES)
	pandoc --template horizontal-lines.pandoc --natbib $(PANDOC_OPTS) $(METADATA) $(FILES) --output $(OUTPUT).tex

# Removes the auxiliary files used to build the report.
clean:
	rm -rf *.aux *.bbl *.blg *.lof *.log *.lot *.out *.tex *.toc

# Removes all generated files, including the report.
mrproper: clean
	rm -rf $(OUTPUT).pdf

# Prints a message describing the available targets.
help:
	@echo
	@echo "Build your Report with Pandoc"
	@echo "============================="
	@echo
	@echo "Available targets are:"
	@echo "    - $(OUTPUT).pdf: builds the report (default)"
	@echo "    - $(OUTPUT).tex: converts from Markdown to LaTeX"
	@echo "    - clean: removes auxiliary files"
	@echo "    - mrproper: removes all generated files"
	@echo "    - help: displays this help"
	@echo
