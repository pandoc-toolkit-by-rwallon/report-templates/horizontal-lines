---
# Document Configuration.
papersize:
fontsize:
inputenc:
language:
geometry:
    -

# Preamble Customization.
packages:
    -
header-includes:
    -

# Color Definitions.
color:
    - name:
      type:
      value:

# Color Settings.
urlcolor:
citecolor:
linkcolor:
section-color:
subsection-color:
subsubsection-color:
paragraph-color:
subparagraph-color:

# Font Settings.
fontfamily:
fontfamilyoptions:
    -

# Section Customization.
numbersections:
secnumdepth:
subparagraph:

# Miscellaneous.
linestretch:
links-as-notes:

# Document Metadata.
title:
title-meta:
subtitle:
date:
author:
    -
author-meta:
institute:
    -
keywords:
    -

# Cover and Title Page.
notitle:
cover:
logos:
    -
logo-height:

# Front Matter.
toc:
toc-depth:
lot:
lof:

# Bibliography.
natbiboptions:
biblio-style:
bibliography:
    -
---
