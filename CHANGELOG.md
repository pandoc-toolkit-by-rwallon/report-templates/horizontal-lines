# Changelog

This file describes the evolution of the report template for Pandoc.

Note that versions are numbered using the `BREAKING.FEATURE.FIX` scheme.

## Version 0.1.0 (January 2020)

+ Document configuration can be set from YAML.
+ Document metadata can be set from YAML.
+ Title page may be customized from YAML.
+ Colors may be customized from YAML.
+ Fonts may be customized from YAML.
+ Section titles may be customized from YAML.
+ Front matter may be customized from YAML.
+ Bibliography may be customized from YAML.
+ Reports can be built using `make`.
