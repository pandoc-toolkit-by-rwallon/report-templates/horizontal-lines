# Use Cases of the Report Template

This directory contains examples illustrating different use cases of the Pandoc
template for writing reports.

All the examples are based on the same Markdown source, available
[here](example.md).
The different outputs are only obtained by using different metadata files.

For each of the examples, you may either read its metadata file (to see how we
configured it) or see the final PDF.

## Default Configuration

In this example, there are no additional settings.
The report is produced based on the default configuration.

*Metadata file available [here](default.md).*

*PDF available [here](/../builds/artifacts/master/file/examples/default.pdf?job=make-examples).*

## Custom Style

In this example, the main font of the document is modified to use `libertinus`
instead of the default `lmodern` font, and the elements of the document use a
custom set of colors.
Also, a custom cover is set as first page of the document, instead of the
default title page.

*Metadata file available [here](custom-style.md).*

*PDF available [here](/../builds/artifacts/master/file/examples/custom-style.pdf?job=make-examples).*

## Custom Content

This example adds a table of contents at the beginning of the report and uses
a custom style for the bibliography.
Moreover, sections are numbered and links are displayed as foot notes.
Also, a custom cover is set as first page of the document, in addition to the
default title page.

*Metadata file available [here](custom-content.md).*

*PDF available [here](/../builds/artifacts/master/file/examples/custom-content.pdf?job=make-examples).*
