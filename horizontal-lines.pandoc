% ----------------------------------------------------------------------------
% Pandoc Template for Writing Reports with LaTeX.
% Copyright (c) 2020 - Romain Wallon.
%
% This template uses a title page by Arslan 'rcx' Atajanov, available at
% https://overleaf.com/latex/templates/project-template-titlepage/bwmhgfdvvhpw.
%
% This work is licensed under a Creative Commons Attribution 4.0 International
% License (https://creativecommons.org/licenses/by/4.0/).
% ----------------------------------------------------------------------------

% -------------------------- PACKAGE CONFIGURATION ---------------------------

\PassOptionsToPackage{unicode=true}{hyperref}
\PassOptionsToPackage{hyphens}{url}
\PassOptionsToPackage{dvipsnames,svgnames*,x11names*}{xcolor}

% -------------------------- DOCUMENT CONFIGURATION --------------------------

\documentclass[$if(papersize)$$papersize$$else$a4$endif$paper,$if(fontsize)$$fontsize$$endif$]{report}

\usepackage[$if(inputenc)$$inputenc$$else$utf8$endif$]{inputenc}

\usepackage[$if(language)$$language$$else$english$endif$]{babel}

\usepackage[$for(geometry)$$geometry$$sep$,$endfor$]{geometry}

% ------------------------- LOADING USEFUL PACKAGES --------------------------

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}

\usepackage{booktabs}
\usepackage{longtable}
\usepackage{tabularx}

\usepackage{titlesec}
\usepackage{xcolor}

$if(cover)$
\usepackage{pdfpages}
$endif$

$for(packages)$
\usepackage{$packages$}
$endfor$

% ---------------------------- COLOR DEFINITIONS -----------------------------

$for(color)$
\definecolor{$color.name$}{$color.type$}{$color.value$}
$endfor$

% -------------------------------- FONT STYLE --------------------------------

$if(fontfamily)$
\usepackage[$for(fontfamilyoptions)$$fontfamilyoptions$$sep$,$endfor$]{$fontfamily$}
$else$
\usepackage{lmodern}
$endif$

% -------------------------- SECTION CUSTOMIZATION ---------------------------

\renewcommand{\thesection}{\Roman{section}}
\titleformat{\section}
    {\LARGE\bfseries$if(section-color)$\color{$section-color$}$endif$}
    {$if(numbersections)$\Roman{section}.$endif$}
    {$if(numbersections)$10pt$else$0pt$endif$}
    {}

\renewcommand{\thesubsection}{\alph{subsection}}
\titleformat{\subsection}
    {\Large\bfseries$if(subsection-color)$\color{$subsection-color$}$endif$}
    {$if(numbersections)$\alph{subsection}.$endif$}
    {$if(numbersections)$10pt$else$0pt$endif$}
    {}

\renewcommand{\thesubsubsection}{\arabic{subsubsection}}
\titleformat{\subsubsection}
    {\large\bfseries$if(subsubsection-color)$\color{$subsubsection-color$}$endif$}
    {$if(numbersections)$\arabic{subsubsection}.$endif$}
    {$if(numbersections)$10pt$else$0pt$endif$}
    {}

\renewcommand{\theparagraph}{\Alph{paragraph}}
\titleformat{\paragraph}
    {\normalsize\bfseries$if(paragraph-color)$\color{$paragraph-color$}$endif$}
    {$if(numbersections)$\Alph{paragraph}.$endif$}
    {$if(numbersections)$10pt$else$0pt$endif$}
    {$if(subparagraph)$$else$\mbox{}$endif$}

\renewcommand{\thesubparagraph}{\roman{subparagraph}}
\titleformat{\subparagraph}
    {\normalsize\itshape$if(subparagraph-color)$\color{$subparagraph-color$}$endif$}
    {$if(numbersections)$\roman{subparagraph}.$endif$}
    {$if(numbersections)$10pt$else$0pt$endif$}
    {$if(subparagraph)$$else$\mbox{}$endif$}

\setcounter{secnumdepth}{$if(secnumdepth)$$secnumdepth$$else$5$endif$}

\newcommand{\sectionbreak}{\cleardoublepage}

% -------------------------------- HYPERLINKS --------------------------------

\usepackage{hyperref}

$if(links-as-notes)$
\DeclareRobustCommand{\href}[2]{#2\footnote{\url{#1}}}
$endif$

% --------------------------- SYNTAX HIGHLIGHTING ----------------------------

\usepackage{listings}

\newcommand{\passthrough}[1]{#1}

\lstset{defaultdialect=[5.3]Lua}
\lstset{defaultdialect=[x86masm]Assembler}

\lstnewenvironment{code}{\lstset{language=Haskell,basicstyle=\small\ttfamily}}{}

$highlighting-macros$

$if(verbatim-in-note)$
\usepackage{fancyvrb}
\VerbatimFootnotes
$endif$

% --------------------------- FIGURE CUSTOMIZATION ---------------------------

\usepackage{float}
\usepackage{graphicx}
\usepackage{grffile}

\makeatletter

\def \maxwidth {
    \ifdim \Gin@nat@width > \linewidth
        0.8\linewidth
    \else
        \Gin@nat@width
    \fi
}

\def \maxheight {
    \ifdim \Gin@nat@height > \textheight
        0.4\textheight
    \else
        \Gin@nat@height
    \fi
}

\makeatother

\setkeys{Gin}{width=\maxwidth,height=\maxheight,keepaspectratio}
\let\originalfigure\figure
\let\originalendfigure\endfigure
\renewenvironment{figure}{\originalfigure[H]}{\originalendfigure}

% ------------------------ BIBLIOGRAPHY CUSTOMIZATION ------------------------

$if(bibliography)$
\usepackage[$for(natbiboptions)$$natbiboptions$$sep$,$endfor$]{natbib}
\bibliographystyle{$if(biblio-style)$$biblio-style$$else$plainnat$endif$}
$endif$

% ---------------------------- PANDOC INTEGRATION ----------------------------

$if(linestretch)$
\usepackage{setspace}
\setstretch{$linestretch$}
$endif$

\setlength{\emergencystretch}{3em}
\providecommand{\tightlist}{\setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}

$if(strikeout)$
\usepackage[normalem]{ulem}
\pdfstringdefDisableCommands{\renewcommand{\sout}{}}
$endif$

$for(header-includes)$
$header-includes$
$endfor$

\newcolumntype{Y}{>{\centering\arraybackslash}X}

% ----------------------------- DOCUMENT METADATA ----------------------------

\hypersetup{
$if(title-meta)$
    pdftitle={$title-meta$},
$endif$
$if(author-meta)$
    pdfauthor={$author-meta$},
$endif$
$if(keywords)$
    pdfkeywords={$for(keywords)$$keywords$$sep$,$endfor$},
$endif$
$if(colorlinks)$
    colorlinks=true,
    linkcolor=$if(linkcolor)$$linkcolor$$else$Maroon$endif$,
    citecolor=$if(citecolor)$$citecolor$$else$Blue$endif$,
    urlcolor=$if(urlcolor)$$urlcolor$$else$Blue$endif$,
$else$
    pdfborder={0 0 0},
$endif$
    breaklinks=true
}

\title{$title$}
\date{$if(date)$$date$$else$\today$endif$}
\author{$for(author)$$author$$sep$\\$endfor$}

% ---------------------------------- DOCUMENT --------------------------------

\begin{document}

$if(cover)$
    % ------------ %
    % Custom Cover %
    % ------------ %

    {
        \newgeometry{margin=0mm}
        \includepdf[pages=1,height=\textheight]{$cover$}
    }

    \newpage

    \thispagestyle{empty}
    \mbox{}
    \newpage
$endif$

$if(notitle)$
$else$
    % ---------- %
    % Title Page %
    % ---------- %

    \thispagestyle{empty}

    \begin{titlepage}
        \centering

        \vfill

$for(institute)$
        \Large\textsc{$institute$}$sep$\\
$endfor$

        \vfill

        \rule{\linewidth}{.2mm}\\[4mm]
        {\huge\bfseries $title$}\\
$if(subtitle)$
        \vspace{4mm}
        {\large\bfseries $subtitle$}\\
$endif$
        \rule{\linewidth}{.2mm}\\[15mm]

        \normalsize
        $if(date)$$date$$else$\today$endif$

        \vfill

$if(logos)$
        \begin{tabularx}{\textwidth}{$for(logos)$Y$endfor$}
$for(logos)$
            \includegraphics[height=$if(logo-height)$$logo-height$$else$4cm$endif$]{$logos$}$sep$&
$endfor$
        \end{tabularx}
$endif$

        \vfill

        $for(author)$$author$$sep$, $endfor$\\
    \end{titlepage}

    \newpage

    \thispagestyle{empty}
    \mbox{}
    \newpage

    \newpage
$endif$

$if(toc)$
    % ----------------- %
    % Table of Contents %
    % ----------------- %

    $if(toc-depth)$\setcounter{tocdepth}{$toc-depth$}$endif$
    \tableofcontents

    \cleardoublepage

    \ifodd\value{page}
        \newpage
        \thispagestyle{empty}
        \mbox{}
    \fi
$endif$

$if(lot)$
    % -------------- %
    % List of Tables %
    % -------------- %

    \listoftables

    \cleardoublepage

    \ifodd\value{page}
        \newpage
        \thispagestyle{empty}
        \mbox{}
    \fi
$endif$

$if(lof)$
    % --------------- %
    % List of Figures %
    % --------------- %

    \listoffigures

    \cleardoublepage

    \ifodd\value{page}
        \newpage
        \thispagestyle{empty}
        \mbox{}
    \fi
$endif$

    % ------------ %
    % Main Content %
    % ------------ %

$body$

    % ------------ %
    % Bibliography %
    % ------------ %

$if(bibliography)$
    \bibliography{$for(bibliography)$$bibliography$$sep$,$endfor$}
$endif$

\end{document}
