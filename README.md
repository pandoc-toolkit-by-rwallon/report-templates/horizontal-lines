# Pandoc Template for Writing Reports

[![pipeline status](https://gitlab.com/pandoc-toolkit-by-rwallon/report-templates/horizontal-lines/badges/master/pipeline.svg)](https://gitlab.com/pandoc-toolkit-by-rwallon/report-templates/horizontal-lines/commits/master)

## Description

This project provides a template for writing reports with a smooth integration
with [Pandoc](https://pandoc.org).
It uses a title page inspired by that of Arslan 'rcx' Atajanov, available on
[Overleaf](https://overleaf.com/latex/templates/project-template-titlepage/bwmhgfdvvhpw).

This template is distributed under a Creative Commons Attribution 4.0
International License.

[![CC-BY 4.0](https://i.creativecommons.org/l/by/4.0/88x31.png)](https://creativecommons.org/licenses/by/4.0/)

## Requirements

To build your report using this template, [Pandoc](https://pandoc.org)
at least 2.0 and [LaTeX](https://www.latex-project.org/) have to be installed
on your computer.

## Creating your Report

This template enables to take advantage of the power of Pandoc to write your
reports in plain [Markdown](https://pandoc.org/MANUAL.html#pandocs-markdown).

The template we provide supports many customizations, so that you can create
your own reports without having to write LaTeX code.

You will find below a description of the available customizations.
Most of them are standard Pandoc options, and their description is taken
from [Pandoc's documentation](https://pandoc.org/MANUAL.html).
You may also be interested in our [examples](./examples), which illustrate
various use cases.

### Document Configuration

You may configure your document using the following options:

+ `papersize`: the paper size (default is `a4`).
+ `fontsize`: the font size for the body text (e.g. `10pt` or `12pt`).
+ `inputenc`: the charset encoding of the document (default is `utf8`).
+ `language`: the language of the document (default is `english`).
+ `geometry`: the list of options for the `geometry` package
  (e.g. `margin=1in`).

### Preamble Customization

You may add LaTeX settings to the preamble using the following options:

+ `packages`: the names of the additional packages to load.
+ `header-includes`: a list of (raw) LaTeX commands to add to the preamble.

### Colors

#### Color Definitions

You may define your own colors through the variable `color`, which must be
set to a list of objects defining three fields:

+ `name`: the name of the color you are defining.
+ `type`: the type of the color, among `gray`, `rgb`, `RGB`, `html`, `HTML`,
  etc.
+ `value`: the actual value of the color.

Note that each color is then automatically translated into LaTeX by using the
following command:

```latex
\definecolor{name}{type}{value}
```

#### Color Settings

Once you have defined your own colors, you may set the following options to
customize the colors to use in the report (of course, you may also use
predefined colors):

+ `urlcolor`: the color for the URLs.
+ `citecolor`: the color for the citations.
+ `linkcolor`: the color for the links.
+ `section-color`: the color for the title of the sections.
+ `subsection-color`: the color for the title of the subsections.
+ `subsubsection-color`: the color for the title of the subsubsections.
+ `paragraph-color`: the color for the title of the paragraphs.
+ `subparagraph-color`: the color for the title of the subparagraphs.

### Font Settings

You may set the following options to customize the fonts to use in the report:

+ `fontfamily`: the font family for the body text.
+ `fontfamilyoptions`: the list of options for the package defined as
  `fontfamily`.

### Section Customization

You may customize the appearance of the section titles using the following
options:

+ `numbersections`: whether sections must be numbered.
+ `secnumdepth`: the numbering depth for the sections.
+ `subparagraph`: whether paragraphs and subparagraphs must be displayed
  as such (if not set, they are displayed like sections).

### Document Metadata

You may set the metadata of your report with the following options:

+ `title`: the title of the report.
+ `title-meta`: the title to set in the output PDF metadata (by default, the
  value of `title` is used).
+ `subtitle`: the subtitle of the report.
+ `date`: the date of the report (by default, the current date is used).
+ `author`: the list of the authors of the report.
+ `author-meta`: the authors to set in the output PDF metadata (by default, the
  values of `author` are used).
+ `institute`: the list of the institutes of the authors.
+ `keywords`: the list of keywords for the report.

### Cover and Title Page

You may customize the appearance of the cover and title page of the report
with the following options:

+ `notitle`: whether the default title page must not be used.
+ `cover`: the custom cover to use (if any).
+ `logos`: the list of logos to display on the title page.
+ `logo-height`: the height of the logos on the title page (width is divided
  equally between all logos).

### Front Matter

You may configure your front matter using the following options:

+ `toc`: includes a table of contents.
+ `toc-depth`: the level of sections to include in the table of contents.
+ `lof`, `lot`: includes the list of figures and the list of tables.

### Bibliography

Your report may contain bibliography references, managed using
[Natbib](https://www.ctan.org/pkg/natbib).
You may set up the bibliography configuration using the following options:

+ `natbiboptions`: the list of options for Natbib.
+ `biblio-style`: the bibliography style.
+ `bibliography`: the bibliography to use for resolving references (as a list
  of BibTeX files).

### Miscellaneous

Some other options may be set to customize your report, namely:

+ `linestretch`: adjusts line spacing using the `setspace` package
  (e.g. `1.25` or `1.5`).
+ `links-as-notes`: causes links to be printed as footnotes.

### Building your Report

Reports based on this template are built using `make`.
To customize the build to your own use case, you may either update the
[`Makefile`](./Makefile) provided in this project, or set the needed variables
while building.

For example, suppose that your Markdown source file is `example.md` and your
metadata file is `metadata.md`.
You will then have to type the following command to build your report in a file
`example.pdf`:

```bash
make METADATA=metadata.md FILES=example.md OUTPUT=example
```

Note that you may also set the program to use to create the PDF from LaTeX
source with `PDF_ENGINE` (default is `xelatex`) and customize the way Pandoc
produces the report by setting the variable `PANDOC_OPTS` accordingly.
For more details, read [Pandoc's User Guide](https://pandoc.org/MANUAL.html).

Our `Makefile` also provides `make clean`, which removes all generated files
but the PDF of the report, and `make mrproper`, which also removes this PDF.

Type `make help` to have the full list of available targets.
